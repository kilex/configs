# Playbooks

## check_sudo.yml
Проверка работсоспособности sudo

## libssl.yml
Обновление libssl и рестарт nginx если есть обновление

## debian_version.yml
Определение версии debian

## musthave.yml
Установка некоторых [Must Have](https://bitbucket.org/kilex/configs/wiki/linux/mh) tools

## nmap.yml
Установка nmap без иксов

## ping.yml
Проверялка доступности всех серверов

## mongo.yml
Установщик монги и рокмонги (пароль к рокмонге указывается внутри плейбука)

## swapfile.yml
Создание файла swap. Удобно для вируталок на DigitalOcean