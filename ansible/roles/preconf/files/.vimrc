"filetype plugin on
set showtabline=2

set tabstop=4
set shiftwidth=4
set smarttab
"set expandtab
set autoindent
set smartindent
au BufRead,BufNewFile *.md set filetype=markdown

"set foldenable
"set foldmethod=manual
"set foldmarker={,}
"set foldcolumn=1
set incsearch
set hlsearch

if version >= 730

	" Если нет папки для отмены - создаем
	function! Mkdir()
		let l:undodir = $HOME.'/.vim/undo'
		if !isdirectory(l:undodir)
			call mkdir(l:undodir, 'p', 0700)
		endif
	endfunction
	
	call Mkdir()

    set history=64
    set undolevels=128
    set undodir=~/.vim/undo
    set undofile
    set undolevels=1000
    set undoreload=10000
endif

" Show numbers on string
"set number
"set t_Co=256
"colorscheme calmar256-dark
"colo xterm16
"set makeprg=/usr/share/vim/current/tools/efm_perl.pl\ -c\ %\ $*


syntax on
"colorscheme murphy

set fileencodings=utf-8,koi8-r
set fileformats=unix,dos,mac
