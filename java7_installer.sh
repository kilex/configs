#!/bin/bash
wget -c --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/7u60-b19/jdk-7u60-linux-x64.tar.gz" -O /tmp/jdk-7u60-linux-x64.tar.gz
wget -c --no-cookies --no-check-certificate "http://ppa.launchpad.net/webupd8team/java/ubuntu/pool/main/o/oracle-java7-installer/oracle-java7-installer_7u60-0~webupd8~0_all.deb" -O /tmp/oracle-java7-installer_7u60.deb
echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
echo oracle-java7-installer oracle-java7-installer/local select /tmp  | sudo /usr/bin/debconf-set-selections
apt-get install java-common
#echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu precise main" > /etc/apt/sources.list.d/webupd8team-java.list
#echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu precise main" >> /etc/apt/sources.list.d/webupd8team-java.list
#apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886
#apt-get update
#apt-get install oracle-java7-installer
dpkg -i /tmp/oracle-java7-installer_7u60.deb
